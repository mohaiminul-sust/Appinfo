//
//  ViewController.swift
//  appinfo
//
//  Created by Mohaiminul Islam on 2/14/16.
//  Copyright © 2016 infancyit. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var idField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //delegate
        idField.delegate = self as? UITextFieldDelegate
        descriptionView.delegate = self as? UITextViewDelegate
    }
    
    @IBAction func getButton(sender: AnyObject) {
        
        guard let id = idField.text else {
            return self.relayMessage("id pay nai")
        }
        
        //alamofire get request with json response
        Alamofire.request(.GET, "http://ratelancer.com:3000/appinfo/\(id)")
            .responseJSON { response in

                if let data = response.result.value {
                    //debug data
                    print("JSON: \(data)")
                    
                    //setting data to view components
                    self.titleLabel.text = data.objectForKey("title") as? String
                    self.descriptionView.text = data.objectForKey("description") as? String
                }
        }
        
    }
    
    func relayMessage(message: String){
        print("Error : \(message)")
    }
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}

